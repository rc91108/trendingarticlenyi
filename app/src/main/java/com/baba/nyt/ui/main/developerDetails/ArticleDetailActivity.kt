package com.baba.nyt.ui.main.developerDetails

import android.os.Bundle
import com.baba.nyt.BR
import com.baba.nyt.base.BaseActivity
import com.baba.nyt.R
import com.baba.nyt.databinding.ActivityArticleDetailBinding
import com.baba.nyt.ui.main.model.TrendingArticleDataModel
import com.baba.nyt.utils.Constant
import javax.inject.Inject

class ArticleDetailActivity : BaseActivity<ActivityArticleDetailBinding, ArticleDetailViewModel>() {

    @Inject
    lateinit var articleDetailViewModel: ArticleDetailViewModel

    override fun getBindingVariable(): Int {
        return BR.detailviewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_article_detail
    }

    override fun getViewModel(): ArticleDetailViewModel {
        return articleDetailViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setData()
    }

    private fun setData() {
        val bundle = intent.getBundleExtra(Constant.IntentKey.MY_BUNDLE)
        val trendingArticleDataModel = bundle?.getParcelable<TrendingArticleDataModel>(Constant.IntentKey.MEDAI_DETAIL) as TrendingArticleDataModel
        articleDetailViewModel.articleModel.apply {
            ctitle = trendingArticleDataModel.title
            cbyline = trendingArticleDataModel.byline
            ctime = trendingArticleDataModel.published_date
            ctype = trendingArticleDataModel.type
            cabstract = trendingArticleDataModel.abstract
        }
    }
}