package com.baba.nyt.ui.main.viewModel

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import com.baba.nyt.base.BaseViewModel
import com.baba.nyt.retrofit.RetrofitRestClient
import com.baba.nyt.ui.main.interfacess.TrendingArticleNavigator
import com.baba.nyt.ui.main.model.TrendingArticleDataModel
import com.baba.nyt.ui.main.model.MainResponseModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

open class TrendingArticleViewModel : BaseViewModel<TrendingArticleNavigator>() {

    val TAG = "TrendingArticleViewMod"
    val devListModel = ObservableArrayList<TrendingArticleDataModel>()
    val articleRepos: MutableLiveData<List<TrendingArticleDataModel>> = MutableLiveData()

    fun addTrendingDevToList(openSourceItems: List<TrendingArticleDataModel>) {
        devListModel.clear()
        devListModel.addAll(openSourceItems)
    }

    fun getTrendingDevData(period: Int, appID: String) {
        setIsLoading(true)
        getEventObservable(period, appID).subscribe(
                { apiResponse ->
                    setIsLoading(false)
                    apiResponse.let {
                        articleRepos.value = it.results
                        navigator.developersListSuccess()
                    }
                },
                { error ->
                    setIsLoading(false)
                    Log.d(TAG, error.message ?: "onUnknownError")
                }
        ).addTo(compositeDisposable)
    }

    open fun getEventObservable(period: Int, appID: String): Single<MainResponseModel> {
        return RetrofitRestClient.getInstance()
                .getTrendingDevDataApi(period, appID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
