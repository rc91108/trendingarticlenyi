package com.baba.nyt

import com.baba.nyt.ui.main.model.MainResponseModel
import com.baba.nyt.ui.main.model.TrendingArticleDataModel
import com.baba.nyt.ui.main.viewModel.TrendingArticleViewModel
import com.baba.nyt.utils.Constant
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito.`when`

class TrendingArticleUseCaseTest {

    @Test
    fun `trending , useCase execute`() {
        val mock = mock<TrendingArticleViewModel>()
        val list = ArrayList<TrendingArticleDataModel>()
        list.apply {
            add(TrendingArticleDataModel("JakeWharton", "Jake", "", "","fad"))
            add(TrendingArticleDataModel("Chris", "Chris", "", "","asd"))
            add(TrendingArticleDataModel("rok", "rok", "", "","ad"))
        }
        val mainResponseModel = MainResponseModel("OK", 1667, list)

        `when`(mock.getEventObservable(7, Constant.SAMPLE_KEY)).thenReturn(Single.just(mainResponseModel))

        mock.getEventObservable(7, Constant.SAMPLE_KEY)
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(mainResponseModel)

        verify(mock).getEventObservable(7, Constant.SAMPLE_KEY)
    }
}