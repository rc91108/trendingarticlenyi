package com.baba.nyt.retrofit;

import com.baba.nyt.ui.main.model.MainResponseModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {


    @GET("viewed/{period}.json")
    public Single<MainResponseModel> getTrendingDevDataApi(@Path("period") int period, @Query("api-key") String apiKey);


}
