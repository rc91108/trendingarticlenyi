package com.baba.nyt.ui.main.model

data class MainResponseModel(val status : String,val num_results: Int,val results: List<TrendingArticleDataModel>)