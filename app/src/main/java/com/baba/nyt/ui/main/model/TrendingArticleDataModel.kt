package com.baba.nyt.ui.main.model

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.baba.nyt.BR

import kotlinx.android.parcel.Parcelize

@Parcelize
data class TrendingArticleDataModel(var title: String?, var byline: String?, var type: String?, var published_date: String?, var abstract: String?) : Parcelable, BaseObservable() {
    constructor() : this("", "", "", "", "")

    @get:Bindable
    var ctitle: String? = title
        set(value) {
            field = value
            notifyPropertyChanged(BR.ctitle)
        }

    @get:Bindable
    var cbyline: String? = byline
        set(value) {
            field = value
            notifyPropertyChanged(BR.cbyline)
        }

    @get:Bindable
    var ctype: String? = type
        set(value) {
            field = value
            notifyPropertyChanged(BR.ctype)
        }

    @get:Bindable
    var ctime: String? = published_date
        set(value) {
            field = value
            notifyPropertyChanged(BR.ctime)
        }

    @get:Bindable
    var cabstract: String? = abstract
        set(value) {
            field = value
            notifyPropertyChanged(BR.cabstract)
        }
}

