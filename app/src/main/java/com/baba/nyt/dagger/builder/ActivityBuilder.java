package com.baba.nyt.dagger.builder;


import com.baba.nyt.ui.main.MainActivity;
import com.baba.nyt.ui.main.developerDetails.ArticleDetailActivity;
import com.baba.nyt.ui.main.developerDetails.module.DevDetailActivityModule;
import com.baba.nyt.ui.main.module.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {DevDetailActivityModule.class})
    abstract ArticleDetailActivity bindDevDetailActivity();
}