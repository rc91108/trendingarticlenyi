package com.baba.nyt.ui.main.module;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baba.nyt.ui.main.MainActivity;
import com.baba.nyt.ui.main.viewModel.TrendingArticleViewModel;
import com.baba.nyt.ui.main.trending.adapter.TrendingArticleAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {
    @Provides
    TrendingArticleViewModel provideDevViewModel() {
        return new TrendingArticleViewModel();
    }

    @Provides
    TrendingArticleAdapter provideMediaAdapter() {
        return new TrendingArticleAdapter();
    }


    @Provides
    RecyclerView.LayoutManager providRecyclerLayoutManager(MainActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
