# NYT 

This repository contains a detailed sample app that implements MVVM architecture using Dagger2, RxJava, Kotlin, Databinding


#### The app has following packages:
1. **base**: Having all base class.
2. **dagger**: Dependency providing classes using Dagger2.
3. **retrofit**: Network related file and error handling codes.
4. **databinding**: Binding utils
5. **ui.main**: View classes along with their corresponding ViewModel.
6. **utils**: Utility classes.

#### Classes have been designed in such a way that it could be inherited and maximize the code reuse.

### Library resources:
1. RxJava2
2. Dagger2

## Developing

### Command Line

1. **Clone the repo**
* `git clone --git clone https://rc91108@bitbucket.org/rc91108/trendingarticlenyi.git`
2. Create, or use an existing, Optimizely Android project
3. Build the project (from the project root)
* `./gradlew assemble`
4. Run tests for all modules
* `./gradlew testAllModules`
* A device or emulator must be connected
5. Install the test app onto all connected devices and emulators
* `./gradlew test-app:installDebug`
* The test app depends on all of the other project modules
* The modules are built from source
* Changes in any modules source will be applied to the test app on the next build
6.  Discover more gradle tasks
* `./gradlew tasks`
* To see the task of an individual module
* `./gradlew user-profile:tasks`

#### Easy Access
Just make pull request. You are in!

