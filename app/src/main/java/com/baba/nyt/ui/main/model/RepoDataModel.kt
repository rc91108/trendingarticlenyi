package com.baba.nyt.ui.main.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RepoDataModel(var byline: String, var description: String, var url: String) : Parcelable
