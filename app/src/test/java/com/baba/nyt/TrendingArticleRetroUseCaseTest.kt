package com.baba.nyt

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.baba.nyt.retrofit.ApiService
import com.baba.nyt.ui.main.model.MainResponseModel
import com.baba.nyt.ui.main.model.TrendingArticleDataModel
import com.baba.nyt.ui.main.viewModel.TrendingArticleViewModel
import com.baba.nyt.utils.Constant
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class TrendingArticleRetroUseCaseTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var trendingArticleViewModel: TrendingArticleViewModel
    private var apiService = providesRetrofit()


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        trendingArticleViewModel = TrendingArticleViewModel()
    }


    @Test
    fun `live data, trending success`() {
        val value = listOf(TrendingArticleDataModel("joshd", "Josh", "user", "", ""))
        trendingArticleViewModel.articleRepos.postValue(value)
        assertEquals(value, (trendingArticleViewModel.articleRepos.value))
    }

    @Test
    fun `live data, trending article error`() {
        val value = Throwable("error")
        val mock = mock<TrendingArticleViewModel>()
        Mockito.`when`(mock.getEventObservable(7, Constant.SAMPLE_KEY)).thenReturn(Single.error(value))
    }

    @Test
    fun `check, remote data values`() {
        val list: MutableList<TrendingArticleDataModel> = mutableListOf()
        list.apply {
            add(TrendingArticleDataModel("joshd", "Josh", "user", "11-02-2019", "some Desc"))
            add(TrendingArticleDataModel("Chris", "Chris", "", "18-07-2019", "xyz data"))
            add(TrendingArticleDataModel("Nikola", "Nikola Irinchev", "", "25-11-2019", "ajhbf"))
        }
        val mainResponseModel = MainResponseModel("OK", 1667, list)

        val trendingArticleViewModel: TrendingArticleViewModel = mock()
        given { trendingArticleViewModel.getEventObservable(7, Constant.SAMPLE_KEY) }.willReturn(Single.just(mainResponseModel))

        trendingArticleViewModel.getEventObservable(7, Constant.SAMPLE_KEY)
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(mainResponseModel)

        verify(trendingArticleViewModel).getEventObservable(7, Constant.SAMPLE_KEY)
    }


    @Test
    fun `network request`() {
        val result = apiService.getTrendingDevDataApi(7, Constant.SAMPLE_KEY)
                .map { data ->
                    data.results.map {
                        "title: ${it.title}, byline: ${it.byline}, type: ${it.type}," +
                                " published_date: ${it.published_date}, abstract :${it.abstract}\n  "
                    }
                }.blockingGet()
        print(result)
    }


    private fun providesRetrofit(): ApiService {
        return Retrofit.Builder()
                .baseUrl("http://api.nytimes.com/svc/mostpopular/v2/")
                .client(OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService::class.java)
    }
}