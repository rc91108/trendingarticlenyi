package com.baba.nyt.ui.main.developerDetails.module;

import com.baba.nyt.ui.main.developerDetails.ArticleDetailViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class DevDetailActivityModule {
    @Provides
    ArticleDetailViewModel provideDetailViewModel() {
        return new ArticleDetailViewModel();
    }


}
