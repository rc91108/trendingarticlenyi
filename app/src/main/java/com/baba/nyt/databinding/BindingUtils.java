package com.baba.nyt.databinding;


import android.text.TextUtils;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.baba.nyt.ui.main.model.TrendingArticleDataModel;
import com.baba.nyt.ui.main.trending.adapter.TrendingArticleAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BindingUtils {

    @BindingAdapter("adapter")
    public static void addMediaItems(RecyclerView recyclerView, List<TrendingArticleDataModel> openSourceItems) {

        TrendingArticleAdapter adapter = (TrendingArticleAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.addData(openSourceItems);
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl))
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .into(view);
    }

}
