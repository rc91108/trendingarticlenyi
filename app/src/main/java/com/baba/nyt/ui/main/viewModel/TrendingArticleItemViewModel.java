package com.baba.nyt.ui.main.viewModel;

import androidx.databinding.ObservableField;

import com.baba.nyt.ui.main.model.TrendingArticleDataModel;

public class TrendingArticleItemViewModel {

    public final DevViewModelListener eventViewModelListener;

    public final ObservableField<String> devTitle = new ObservableField<>();

    public final ObservableField<String> devImage = new ObservableField<>();
    public final ObservableField<String> devByline = new ObservableField<>();
    public final ObservableField<String> devType = new ObservableField<>();
    public final ObservableField<String> devTime = new ObservableField<>();

    private final TrendingArticleDataModel devModel;


    public TrendingArticleItemViewModel(TrendingArticleDataModel devModel, DevViewModelListener eventViewModelListener) {
        this.devModel = devModel;
        this.devTitle.set(devModel.getTitle());
        this.devByline.set(devModel.getByline());
        this.devTime.set(devModel.getPublished_date());
        this.devType.set(devModel.getType());
        this.eventViewModelListener = eventViewModelListener;
    }

    public void onItemClick() {
        eventViewModelListener.onItemClick(devModel);
    }


    public interface DevViewModelListener {

        void onItemClick(TrendingArticleDataModel event);

    }
}
