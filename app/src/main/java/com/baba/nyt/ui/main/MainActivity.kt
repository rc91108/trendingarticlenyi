package com.baba.nyt.ui.main

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.baba.nyt.BR
import com.baba.nyt.R
import com.baba.nyt.base.BaseActivity

import com.baba.nyt.databinding.ActivityMainBinding
import com.baba.nyt.ui.main.interfacess.TrendingArticleNavigator
import com.baba.nyt.ui.main.trending.adapter.TrendingArticleAdapter
import com.baba.nyt.ui.main.viewModel.TrendingArticleViewModel
import com.baba.nyt.utils.Constant
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import javax.inject.Provider

class MainActivity : BaseActivity<ActivityMainBinding, TrendingArticleViewModel>(), TrendingArticleNavigator {

    @Inject
    lateinit var trendingArticleViewModel: TrendingArticleViewModel

    @Inject
    internal lateinit var trendingArticleAdapter: TrendingArticleAdapter


    @Inject
    lateinit var mLayoutManager: Provider<RecyclerView.LayoutManager>

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): TrendingArticleViewModel? {
        return trendingArticleViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trendingArticleViewModel.navigator = this
        recyclerViewSetUp()
        subscribeToLiveData()

        trendingArticleViewModel.getTrendingDevData(7, Constant.SAMPLE_KEY)
    }

    private fun recyclerViewSetUp() {
        trendingDevRecycler.apply {
            layoutManager = mLayoutManager.get()
            adapter = trendingArticleAdapter
            addItemDecoration(DividerItemDecoration(this@MainActivity,
                    DividerItemDecoration.VERTICAL))
        }
    }

    private fun subscribeToLiveData() {
        trendingArticleViewModel.articleRepos.observe(this,
                Observer { trendingArticleViewModel.addTrendingDevToList(it!!) })

    }

    override fun developersListSuccess() {
    }

}
