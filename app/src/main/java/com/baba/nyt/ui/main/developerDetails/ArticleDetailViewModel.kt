package com.baba.nyt.ui.main.developerDetails

import com.baba.nyt.base.BaseViewModel
import com.baba.nyt.ui.main.interfacess.BaseNavigation
import com.baba.nyt.ui.main.model.TrendingArticleDataModel

class ArticleDetailViewModel : BaseViewModel<BaseNavigation>() {
    var articleModel: TrendingArticleDataModel = TrendingArticleDataModel()
}
