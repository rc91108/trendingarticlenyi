package com.baba.nyt.ui.main.trending.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.baba.nyt.base.BaseViewHolder
import com.baba.nyt.databinding.AdapterTrendingDevBinding
import com.baba.nyt.ui.main.model.TrendingArticleDataModel
import com.baba.nyt.ui.main.developerDetails.ArticleDetailActivity
import com.baba.nyt.ui.main.viewModel.TrendingArticleItemViewModel
import com.baba.nyt.utils.Constant

class TrendingArticleAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    internal var mOpenSourceResponseList: List<TrendingArticleDataModel> = mutableListOf()

    fun addData(articleDataModels: List<TrendingArticleDataModel>) {
        mOpenSourceResponseList = articleDataModels
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemView = AdapterTrendingDevBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.onBind(position, mOpenSourceResponseList)

    override fun getItemCount(): Int = mOpenSourceResponseList.size


    inner class ViewHolder(private val mBinding: AdapterTrendingDevBinding) : BaseViewHolder(mBinding.root),
            TrendingArticleItemViewModel.DevViewModelListener {

        private var articleItemViewModel: TrendingArticleItemViewModel? = null

        override fun onBind(position: Int, dataObject: Any?) {
            articleItemViewModel = TrendingArticleItemViewModel(mOpenSourceResponseList[position], this)
            this.mBinding.trendingDevModel = articleItemViewModel
            this.mBinding.executePendingBindings()
        }

        override fun onItemClick(model: TrendingArticleDataModel) {
            itemView.context
            val mIntent = Intent(itemView.context, ArticleDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable(Constant.IntentKey.MEDAI_DETAIL, model)
            mIntent.putExtra(Constant.IntentKey.MY_BUNDLE, bundle)
            itemView.context.startActivity(mIntent)
        }
    }
}
